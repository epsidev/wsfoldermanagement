package fr.epsi.montpellier.wsfoldermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class WsfoldermanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsfoldermanagementApplication.class, args);
	}

	/** Autorisation CORS
	 *
	 * @return WebMvcConfigurer
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer()
	{
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowCredentials(true)
						.allowedOriginPatterns("*")
						.allowedHeaders("*")
						.allowedMethods("OPTIONS", "GET", "POST", "PUT", "DELETE");
			}
		};
	}
}
