package fr.epsi.montpellier.wsfoldermanagement.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFoldersInfo {
    private String login;
    private int filesCount;
    private long size;
}
