package fr.epsi.montpellier.wsfoldermanagement.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MQUserMessage {
    private int status;
    private String login;
    private String message;

}
